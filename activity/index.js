const express = require("express")
const app = express()
const mongoose = require("mongoose")

const port = 3000

app.use(express.json())
app.use(express.urlencoded({extended:true}))

mongoose.connect("mongodb+srv://admin:admin1234@b256-cruz.rvtoo2s.mongodb.net/B256_to-do?retryWrites=true&w=majority",{
    useNewUrlParser: true,
	useUnifiedTopology: true
}).then(() => {
    console.log('Were connected to the cloud database');
}).catch((err) => {
    console.error('Error connecting to MongoDB:', err.message);
});


const userSchema = new mongoose.Schema({
    username: { type: String, required: true, default: "pending" },
    password: { type: String, required: true, default: "pending" },
})

const User = mongoose.model("User",userSchema)

app.post("/signup", (req, res) => {
    const newUser = new User ({
        username: req.body.username,
        password: req.body.password
    })
    
    newUser.save()
    .then(() => {
        res.status(201).send("New User registered")
    }).catch((err) => {
        console.error('Error saving user to database:', err.message)
    });
})

app.get("/user", (req, res) => {
    User.find({})
    .then((result) => {
        res.status(200).json({ data: result })
    })
    .catch((err) => {
        console.error("Error fetching users:", err.message)
        res.status(500).json({ error: err.message })
    })
})

app.listen(port, () => console.log(`Server is running at port ${port}`));
